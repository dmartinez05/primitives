#!/usr/bin/env python3

import argparse
import glob
import json
import sys
import traceback


def process_directory(directory):
    known_primitives = {}
    has_errored = False

    for primitive_annotation_path in glob.iglob('{directory}/*/*/*/primitive.json'.format(directory=directory)):
        try:
            with open(primitive_annotation_path, 'r', encoding='utf8') as primitive_annotation_file:
                primitive_annotation = json.load(primitive_annotation_file)

            if primitive_annotation['id'] in known_primitives:
                has_errored = True
                print("Error: Duplicate primitive IDs, '{first_python_path}' and '{second_python_path}'.".format(
                    first_python_path=known_primitives[primitive_annotation['id']]['python_path'],
                    second_python_path=primitive_annotation['python_path'],
                ), flush=True)
            else:
                known_primitives[primitive_annotation['id']] = primitive_annotation
        except Exception:
            print("Error at primitive '{primitive_annotation_path}'.".format(primitive_annotation_path=primitive_annotation_path), flush=True)
            traceback.print_exc()
            sys.stdout.flush()
            has_errored = True

    return has_errored


def main():
    parser = argparse.ArgumentParser(description="Check that primitives have unique IDs.")
    parser.add_argument('directories', metavar='DIR', nargs='*', help="directories to check primitives in", default=())
    arguments = parser.parse_args()

    has_errored = False
    for directory in arguments.directories:
        has_errored = process_directory(directory) or has_errored

    if has_errored:
        sys.exit(1)


if __name__ == '__main__':
    main()
